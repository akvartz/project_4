#!/bin/bash
#
# Version 1.0
#
# Description: basic cybersecurity toolkit for a forgetful mind
#
# Usage:  etk [enter]
#
# By: Kyle Kagawa
#
################################################################################

#x=${1%?}
#date="$(date +%Y%m%d)"
date="$EPOCHSECONDS"
app="$app"
mapt="$mapt"
yon="$yon"
sploit="$sploit"
cve=$CVE
#Function assignment
PS3='Please enter the number next to the desired script or option and press [enter]
Note: Certain programs will continuously run until needed. Please use Ctrl+C to exit back to the prompt.

Your entry: '
echo -e '\n'
options=("Exploit" "John the Ripper" "Aircrack-ng" "Date Timestamper" "Exit")

select app in "${options[@]}"
do
case $app in
"Exploit")
echo "Nmap"
echo -e '\n'
# Nmap simple scanner
# 2 options only. Open port scan or aggressive scan
options=("Port Scan Nmap" "Aggressive Nmap")

	select mapt in "${options[@]}"
	do
	case $mapt in
"Port Scan Nmap")
	read -p "Please designate the IP address to scan and press [enter]

Your Entry: " IP
	IP=${IP// /}
	IP=${IP^^}
echo -e '\n'
	echo "nmap -sV $IP"
	echo "Nmap is now scanning open ports on the IP address..."
# run scan and output log
	mkdir -pm 744 /tmp/ctk/
PNMAP="nmap -sV $IP"
	echo "Nmap Open Port Scan" | tee -a /tmp/ctk/exploit-tool-kit_$date.log
	nmap -sV $IP | tee -a /tmp/ctk/exploit-tool-kit_$date.log

	echo "Searchsploit"
	echo -e '\n'
# run searchsploit based off of the EDB-ID number
	read -p "Enter the EDB-ID number and press [enter]

EDB-ID: " sploit
	sploit=$sploit
	echo ""| tee -a /tmp/ctk/exploit-tool-kit_$date.log
	echo "searchsploit $sploit" | tee -a /tmp/ctk/exploit-tool-kit_$date.log
	searchsploit --color $sploit | tee -a /tmp/ctk/exploit-tool-kit_$date.log
	open /tmp/ctk/exploit-tool-kit_$date.log

echo "Metasploit"
echo -e '\n'
msfconsole -q -x "search --cve 2017-5638; use 0; set RHOSTS $IP; exploit;"

break ;;

"Aggressive Nmap")
	read -p "Please designate the IP address to scan and press [enter]

Your Entry: " IP
	IP=${IP// /}
	IP=${IP^^}
	echo -e '\n'
	echo "nmap -A $IP"
	echo "Nmap is now scanning...aggressively."
# run scan and output log
        mkdir -pm 744 /tmp/ctk/
ANMAP="nmap -A $IP"
		echo "Nmap Agressive Scan" | tee -a /tmp/ctk/exploit-tool-kit_$date.log
        nmap -A $IP | tee -a /tmp/ctk/exploit-tool-kit_$date.log
#OLD	open /tmp/ctk/aggro-nmap_$date.log
	echo "Searchsploit"
	echo -e '\n'
# run searchsploit based off of the EDB-ID number
	read -p "Enter the EDB-ID number and press [enter]

EDB-ID: " sploit
	sploit=$sploit
	echo ""| tee -a /tmp/ctk/exploit-tool-kit_$date.log
	echo "searchsploit $sploit" | tee -a /tmp/ctk/exploit-tool-kit_$date.log
	searchsploit --color $sploit | tee -a /tmp/ctk/exploit-tool-kit_$date.log
	open /tmp/ctk/exploit-tool-kit_$date.log

echo "Metasploit"
echo -e '\n'
msfconsole -q -x "search --cve 2017-5638; use 0; set RHOSTS $IP; exploit;"
break ;;

*) echo "Invalid option $REPLY";;
esac
done
break ;;


"Aircrack-ng")
echo "Aircrack-ng"
echo -e '\n'
aircrack-ng
break ;;


"John the Ripper")
echo "John the Ripper"
echo -e '\n'
# John the Ripper password cracker
options=("Crack Password File" "Show Cracked Passwords")

	select yon in "${options[@]}"
	do
	case $yon in
"Crack Password File")
	read -e -p "Please designate the absolute path to the password file and press [enter]

Your Entry: " passfile
	passfile=$passfile
	echo -e '\n'
# run john and output log
	mkdir -pm 744 /tmp/ctk/
CJOHN="john $passfile"
        echo "john $passfile" | tee -a /tmp/ctk/john-results_$date.log
	echo "john is rippin..."
	john $passfile | tee -a /tmp/ctk/john-results_$date.log
	open /tmp/ctk/john-results_$date.log
break ;;
"Show Cracked Passwords")
	read -e -p "Please designate the absolute path to the password file and press [enter]

Your Entry: " passfile
	passfile=$passfile
	echo -e '\n'
# send output to log file
	mkdir -pm 744 /tmp/ctk/
SJOHN="john --show $passfile"
	echo "john --show $passfile" | tee -a /tmp/ctk/john-show-pass_$date.log
	echo "John is showin..."
	john --show $passfile | tee -a /tmp/ctk/john-show-pass_$date.log
	open /tmp/ctk/john-show-pass_$date.log
break ;;
*) echo "Invalid option $REPLY";;
esac
done
break ;;


"Date Timestamper")
echo "Date Timestamper"
echo -e '\n'
# Date Timestamper rename script
# append date to file name with YYYYMMDD syntax
#
# rename file.log to file_YYYYMMDD.log
# list date and file information before rename
date="$EPOCHSECONDS"
echo "Today's current date is:" $date
echo -e '\n'
echo "Please designate the absolute path to the text file that needs to be renamed.
Here are the contents of the current directory:"
ls
echo -e '\n'
read file
file="$file" 
for f in $file
do
	mkdir -pm 744 /tmp/ctk/temp_rename_log/
	echo "Makin copies..."
	cp -rv $file /tmp/ctk/temp_rename_log/
	echo -e '\n'
        NEW=${file%.log}_$date.log; mv ${file} "${NEW}";
        echo "$file has been appended with _$date"
# text output
echo "$NEW" | tee -a /tmp/ctk/log_rename.log
done
break ;;


"Exit")
echo "Go on, GIT!"
echo -e '\n'
break ;;

*) echo "Invalid option $REPLY";;

esac
done